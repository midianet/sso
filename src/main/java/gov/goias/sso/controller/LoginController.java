package gov.goias.sso.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

    private String


    @GetMapping
    public String login(){
        String token = CookieUtil.token(request, tokenName);
        if(!StringUtils.isEmpty(token)){
            if(authRepository.findByToken(token).isPresent()) {
                return "redirect:/authenticated";
            }
        }
        return "login";
    }

}