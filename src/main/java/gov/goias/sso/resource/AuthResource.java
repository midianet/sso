package gov.goias.sso.resource;

import gov.goias.sso.service.AuthService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Log4j2
@RestController
@RequestMapping(value = "/api/auth", path = "/api/auth")
public class AuthResource {

    @Autowired
    AuthService service;

    @GetMapping("/validate")
    @ResponseStatus(HttpStatus.OK)
    public Boolean validate(HttpServletRequest request){
        return true;
    }

    @PostMapping("/logout")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void logout(HttpServletRequest request) {
        //service.logout();
    }

}