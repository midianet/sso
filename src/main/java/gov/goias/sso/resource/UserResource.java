package gov.goias.sso.resource;

import gov.goias.sso.domain.User;
import gov.goias.sso.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Log4j2
@RestController
@RequestMapping(value = "/api/user", path = "/api/user")
public class UserResource {

    @Autowired
    private UserRepository repository;

    @GetMapping(path = "/{username}")
    @ResponseStatus(HttpStatus.OK)
    public User getByUsername(@PathVariable String username, HttpServletRequest request){
        return repository.findByUsername(username).orElseThrow(() -> new EmptyResultDataAccessException("Username",1));
    }

}