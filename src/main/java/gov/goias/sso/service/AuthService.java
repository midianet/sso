package gov.goias.sso.service;

import gov.goias.sso.domain.Auth;
import gov.goias.sso.domain.User;
import gov.goias.sso.repository.AuthRepository;
import gov.goias.sso.repository.UserRepository;
import gov.goias.sso.util.Util;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.security.AccessControlException;
import java.time.LocalDate;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class AuthService {

    @Autowired
    AuthRepository repository;

    @Autowired
    UserRepository userRepository;

    public Auth login(HttpServletRequest request){
        log.debug("init login");

        String credential = Optional.ofNullable(request.getHeader("Authorization")).orElseThrow(() -> new AccessControlException("Não authenticado...")).trim();
        log.trace("credential %s", credential);

        String address = Optional.ofNullable(Util.getRequestAddress(request)).orElseThrow(() -> new IllegalArgumentException("Endereço Ip inválido ou não presente"));
        String client  = Optional.ofNullable(Util.getRequestClient(request)).orElse("Unidefined");
        log.trace("address %s, client %s",address,client);

        if(!credential.toLowerCase().startsWith("basic")) throw new IllegalArgumentException("Tipo de autenticação inválido");
        credential = credential.substring(5).trim();
        byte[] decoded = Base64.getDecoder().decode(credential);
        String[] identity =  new String(decoded, StandardCharsets.UTF_8).split(":", 2);

        log.trace("find user by Username %s",identity[0]);
        User user = userRepository.findByUsername(identity[0]).orElseThrow(() -> new EmptyResultDataAccessException("Username",1));

        log.debug("compare hash");
        String hash = Util.encriptMD5(identity[1]);
        if(!user.getPassword().equals(hash)) throw new AccessControlException("Usuário e ou senha inválido");

        log.debug("create our reuse Auth");
        var auth = repository.findByAddressAndClientAndLogoutIsNull(address,client);
        if(auth.isPresent()){
            var a = auth.get();
            if(!a.getUser().getUsername().equals(identity[0])){
                a.setLogout(LocalDate.now());
                repository.save(a);
                return create(address,client,user);
            }else{
                return a;
            }
        }else{
            return create(address,client,user);
        }
    }

    private Auth create(String address, String client, User user){
        Auth auth = Auth.builder().login(LocalDate.now()).address(address).client(client).user(user).build();
        repository.save(auth);
        return auth;
    }

    public void logout(String username){
        List<Auth> list = repository.findByUser_UsernameAndLogoutIsNull(username);
        list.forEach(auth -> {
            auth.setLogout(LocalDate.now());
            repository.save(auth);
        });
    }

}