package gov.goias.sso.util;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Optional;

public class Util {

    public static String getRequestAddress(HttpServletRequest request){
        return Optional.ofNullable(request.getHeader("X-FORWARDED-FOR")).orElseGet( ()-> request.getRemoteAddr());
    }

    public static String getRequestClient(HttpServletRequest request){
        return request.getHeader("User-Agent");
    }

    public static String encriptMD5(String value){
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(value.getBytes(), 0, value.length());
            return new BigInteger(1, m.digest()).toString(16);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

}