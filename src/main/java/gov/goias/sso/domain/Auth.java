package gov.goias.sso.domain;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;

@Data
@Builder
public class Auth {

    @Id
    private String id;
    private LocalDate login;
    private LocalDate logout;
    private String address;
    private String client;
    private String token;
    private User user;
}