package gov.goias.sso.domain;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
@Builder
public class User {

    @Id
    private String id;
    private String username;
    private String name;
    private String email;
    private Boolean active;
    private String password;
}