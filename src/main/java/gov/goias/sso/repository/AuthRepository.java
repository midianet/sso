package gov.goias.sso.repository;

import gov.goias.sso.domain.Auth;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface AuthRepository extends MongoRepository<Auth, String> {
    Optional<Auth> findByAddressAndClientAndLogoutIsNull(String address, String client);
    List<Auth> findByUser_UsernameAndLogoutIsNull(String username);
}